/**
 * Created by fabior on 25/04/16.
 */
angular.module('starter.controllers')
    .controller('ClientCheckoutController',['$scope','$state','$cart','$localStorage','ClientOrder','$ionicLoading',
        '$ionicPopup','OAuth','Cupom','$cordovaBarcodeScanner','User',
        function($scope,$state,$cart,$localStorage,ClientOrder,$ionicLoading,
                 $ionicPopup,OAuth,Cupom,$cordovaBarcodeScanner,User){

            var cart = $cart.get();

            $scope.cupom = cart.cupom;
            $scope.items = cart.items;
            $scope.total = $cart.getTotalFinal();

            $scope.removeItem = function (i) {
                $cart.removeItem(i);
                $scope.items.splice(i,1);
                $scope.total = $cart.getTotalFinal();
            }

            $scope.openListProducts = function () {
                $state.go('client.view_products');
            }

            $scope.openProductDetail = function (i) {
                $state.go('client.checkout_item_datail',{index:i});
            }

            $scope.save = function () {
                var o = {items:angular.copy($scope.items)};

                angular.forEach(o.items,function (item) {
                    item.product_id = item.id;
                });

                $ionicLoading.show({
                    template:'Carregando...'
                });
                if($scope.cupom.value){
                    o.cupom_code = $scope.cupom.code;
                }

                ClientOrder.save({id:null},o,function (data) {
                    $ionicLoading.hide();
                    $state.go('client.checkout_successful');
                },function (responseError) {
                    $ionicPopup.alert({
                        title:'Advertência',
                        template:'Pedido não realizado tente novamente.',
                        buttons: [
                            {
                                text: 'Ok',
                                type: 'button-positive',
                                onTap: function(e) {
                                    $state.go('client.checkout')
                                }
                            }
                        ]
                    });
                    $ionicLoading.hide();
                });
            };

            $scope.readBarCode = function () {

                $cordovaBarcodeScanner
                    .scan()
                    .then(function(barcodeData) {
                        // Success! Barcode data is here
                        getValueCupom(barcodeData.text);
                    }, function(error) {
                        // An error occurred
                        $ionicPopup.alert({
                            title:'Advertência',
                            template:'Não foi possivel ler o código de barras - Tente novamente.'
                        });
                    });
            };
            $scope.removeCupom = function () {
                $cart.removeCupom();
                $scope.cupom = $cart.get().cupom;
                $scope.total = $cart.getTotalFinal();
            };
            function getValueCupom(code) {

                $ionicLoading.show({
                    template:'Carregando...'
                });

                Cupom.get({code:code},function (data) {
                    $cart.setCupom(data.data.code,data.data.value);
                    $scope.cupom = $cart.get().cupom;
                    $scope.total = $cart.getTotalFinal();
                    $ionicLoading.hide();
                },function (responseError) {
                    $ionicPopup.alert({
                        title:'Advertência',
                        template:'Cupom inválido.',
                        buttons: [
                            {
                                text: 'Ok',
                                type: 'button-positive',
                                onTap: function(e) {
                                    $state.go('client.checkout')
                                }
                            }
                        ]
                    });
                    $ionicLoading.hide();
                });
            }


    }]);
