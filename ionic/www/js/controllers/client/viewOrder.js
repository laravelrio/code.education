/**
 * Created by fabior on 25/04/16.
 */
angular.module('starter.controllers')
    .controller('ClientViewOrderController',['$scope','$stateParams','ClientOrder','$ionicLoading','$ionicPopup'
        ,function($scope,$stateParams,ClientOrder,$ionicLoading,$ionicPopup){
            $scope.order = {};

            $ionicLoading.show({
                template:'Carregando...'
            });

            ClientOrder.get({id:$stateParams.id,include:'items,cupom'},function (data) {
                $scope.order = data.data[0];
                $ionicLoading.hide();
            },function (dataError) {
                $ionicPopup.alert({
                    title:'Advertência',
                    template:'Erro na conexão.',
                });
                $ionicLoading.hide();
            });
        }]);