/**
 * Created by fabior on 25/04/16.
 */
angular.module('starter.controllers').controller('LoginController',
    ['$scope','$auth',function ($scope,$auth) {

            $scope.user = {
                username:'',
                password:''
            };

            $scope.login = function () {
              $auth.login($scope.user.username,$scope.user.password)
            }
        }]);
