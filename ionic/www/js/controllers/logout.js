/**
 * Created by fabior on 25/04/16.
 */
angular.module('starter.controllers')
    .controller('LogoutController',['$scope','$state','$auth',
        function ($scope,$state,$auth) {
            console.log('SAIR');
            $auth.logout();
            $state.go('login');
    }]);