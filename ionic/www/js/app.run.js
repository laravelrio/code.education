angular.module('starter.run').run([
    '$state','PermissionStore', 'RoleStore', 'OAuth','UserData','$rootScope','authService','httpBuffer',
    function ($state,PermissionStore,RoleStore,OAuth,UserData,$rootScope,authService,httpBuffer) {
        PermissionStore.definePermission('user-permission',function () {
            console.log('isAuthenticate');
            return OAuth.isAuthenticated();
        });

        PermissionStore.definePermission('client-permission',function () {
            var user = UserData.get();
            console.log(user);

            if(user == null || !user.hasOwnProperty('role')){
                return false;
            }
            return user.role == 'client';
        });
        RoleStore.defineRole('client-role',['user-permission','client-permission']);

        PermissionStore.definePermission('deliveryman-permission',function () {
            var user = UserData.get();
            console.log(user);

            if(user == null || !user.hasOwnProperty('role')){
                return false;
            }
            return user.role == 'client';
        });
        RoleStore.defineRole('deliveryman-role',['user-permission','deliveryman-permission']);

        $rootScope.$on('event:auth-loginRequired', function(event, data){
            console.log(data.data.error);
            switch (data.data.error){
                case 'access_denied':
                    if (!$rootScope.refreshingToken) {
                        $rootScope.refreshingToken = OAuth.getRefreshToken();
                    } else {
                        $rootScope.refreshingToken.then(function (data) {
                            authService.loginConfirmed();
                            $rootScope.refreshingToken = null;
                        }, function (responseError) {
                            $state.go('logout');
                        })
                    }
                break;
                case 'invalid_credentials':
                    httpBuffer.rejectAll();
                break;
                default:
                    $state.go('logout');
                break;
            }
        });
}]);