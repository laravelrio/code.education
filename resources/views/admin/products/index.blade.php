@extends('layouts.app')

@section('content')
<div class="container">
    <h3>
        Produtos
    </h3>

    <a href="{{route('admin.products.create')}}" class="btn btn-default">Adicionar</a>
    <br/>
    <br/>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Categoria</th>
            <th>Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
        <tr>
            <td>{{$product->id}}</td>
            <td>{{$product->name}}</td>
            <td>{{$product->category->name}}</td>
            <td>
                <a class="btn btn-info btn-xs" href="{{route('admin.products.edit',$product->id)}}">Editar</a>
                <a class="btn btn-danger btn-xs" href="{{route('admin.products.destroy',$product->id)}}">Excluir</a></td>
        </tr>
        @endforeach
        </tbody>

    </table>

    {!! $products->render() !!}
</div>
@endsection