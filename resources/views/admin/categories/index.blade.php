@extends('layouts.app')

@section('content')
<div class="container">
    <h3>
        Categorias
    </h3>

    <a href="{{route('admin.categories.create')}}" class="btn btn-default">Adicionar</a>
    <br/> <br/>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
        <tr>
            <td>{{$category->id}}</td>
            <td>{{$category->name}}</td>
            <td>
                <a class="btn btn-info btn-xs" href="{{route('admin.categories.edit',$category->id)}}">Editar</a>
                <button class="btn btn-danger btn-xs">Excluir</button></td>
        </tr>
        @endforeach
        </tbody>

    </table>

    {!! $categories->render() !!}
</div>
@endsection