<?php

namespace CodeDelivery\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Fabiorf\IonicPush\PushProcessor',function (){
            return new \Fabiorf\IonicPush\PushProcessor(env('IONIC_APP_ID'),env('IONIC_APP_TOKEN'),env('IONIC_APP_PROFILE'));
        });
    }
}
