<?php
/**
 * Created by PhpStorm.
 * User: fabio
 * Date: 09/05/16
 * Time: 19:53
 */

namespace CodeDelivery\Models;

use Illuminate\Contracts\Support\Jsonable;

class Geo implements Jsonable{

    public $lat;
    public $long;

    public function toJson($options = 0)
    {
         return json_encode([
             'lat' => $this->lat,
             'long' => $this->long,
         ]);
    }

}